/**
 * Created by timeBro on 31.08.16.
 */


var mainACT;
mainACT = new function(){
    $(document).ready(function(){
        $(document).on('click touch', '#clickLeft', function(){
            console.log('clickLeft');
            move('left');
        });

        $(document).on('click touch', '#clickRight', function(){
            console.log('clickRight');
            move('right');
        });

        $("#content").swipe( {
            //Generic swipe handler for all directions
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                //console.log("You swiped " + direction );
                var swipe = 'right';
                if(direction === 'right'){
                   swipe = 'left';
                }
                //console.log('swipe ', swipe);
                move(swipe);
            }
        });

        $(document).on('click touch', '#clickTop', function(){
            //console.log('clicktop');
            $('#button').toggle();
        });

        $(document).on('click touch', '#button', function(){
            capture();
        });
    });
};

function capture(){
    var url = window.location.href + 'capture';
    setTimeout(function(){

    },5000);
    var counter = 5;
    $('#counter').text(counter);
    //sound();
    var interval = setInterval(function() {
        counter--;
        $('#counter').text(counter);
        //sound();
        // Display 'counter' wherever you want to display it.
        if (counter == 0) {
            // Display a login box
            $('#counter').text('');
            $.get(url, function(result){
                console.log('result ', result);
            });
            clearInterval(interval);
        }
    }, 1000);
}

function sound(){
    var audio = new Audio('sounds/beep5.mp3');
    audio.play();
}

var actual = 'initial';
var files = [];
var index = 0;

function move(direction){
    //console.log('direction ', direction);
    //console.log(files);
    //console.log('index', index);
    var ind = false;
    if(direction === 'left' && index > 0){
        index -= 1;
        ind = true;
    }else if(direction === 'right' && index <= files.length-2){
        index += 1;
        ind = true;
    }
    //console.log('index', index);
    if(ind){
        $('#image').attr('src','images/' + files[index]);
    }
}

setInterval(function(){
    //console.log('just checking');
    var url = window.location.href + 'new';
    $.get(url, function(result){
        if(actual !== result.name){
            //console.log('result ',result);
            $('#image').attr('src','images/' + result.name);
            actual = result.name;
            files.push(actual);
            index = files.length - 1 ;
        }else{
            //console.log('nothing new');
        }
    });
}, 100);

