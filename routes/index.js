var express = require('express');
var router = express.Router();
var globals = require('../js/globals');

/* GET home page. */
router.get('/', function(req, res, next) {
  var fs = require("fs");
  fs.readFile(__dirname + '/../public/html/booth.html', 'utf8', function(error, data){
    res.writeHead(200,{"Content-Type" : "text/html"});
    res.write(data);
    res.end();
  });
    //res.sendFile(__dirname + '/../html/booth.html');
});

router.get('/new', function(req, res, next){
  var files = require('../js/files');
  res.json({name: files.newFileName});
});

router.get('/capture', function(req, res, next){
  var global = require('../js/global');
  global.all = false;
  res.json({error: 'none'});
});

module.exports = router;
