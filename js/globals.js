/**
 * Created by timeBro on 31.08.16.
 */

function getAll(){
    var exec = require('child_process').exec;
    var global = require('./global');
    var script = 'cd /home/pi/booth/public/images; gphoto2 -P --skip-existing';
    //console.log('global.all ',global.all);
    if(!global.all){
        console.log('doing other');
        global.all = true;
        script = 'cd /home/pi/booth/public/images; gphoto2 --capture-image-and-download --filename "/home/pi/booth/public/images/photo-%Y%m%d-%H%M%S.jpg"';
        var child = exec(script, function(error, stdout, stderr) {
            if(error){
                //console.log('error ', error);
            }
            setTimeout(function(){
                getAll();
            },2000);
        });
    }else{
        var child = exec(script, function(error, stdout, stderr) {
            if(error){
                //console.log('error ', error);
            }
            getAll();
        });
    }
    //console.log(script);
}
getAll();

var fs = require('fs-extra');
fs.watch('public/images/', function(eventType,file){
    if((file.indexOf('.jpg') !== -1) || (file.indexOf('.JPG') !== -1)){
        //console.log('file ', file);
        var files = require('./files');
        files.newFileName = String(file);
    }
});


